import axios from 'axios'
export const HTTP = axios.create({
  baseURL: self.kioskConfig.API.URL + '/' + self.kioskConfig.API.CATALOG_ID,
  params: {
    token: self.kioskConfig.API.TOKEN
  }
})

export default HTTP
