import { Observable } from 'rxjs'
import { Repo } from '../repo'
import { ProductsLocal } from './ProductsLocal'
import { ProductsRemote } from './ProductsRemote'

export class ProductsRepo extends Repo {
  constructor () {
    super(new ProductsRemote(), new ProductsLocal(), 'products')
  }

  show (productId) {
    return new Observable(async (subscriber) => {
      // get local product and show it
      try {
        let product = await this.local.show(productId)
        // console.log('local product', product)
        subscriber.next(product)
        // get remote product and show
        product = await this.remote.show(productId).then(response => {
          // console.log('remote response', response)
          return response.data.product
        })
        // console.log('remote product', product)
        subscriber.next(product)
        // save product
        this.local.save(product)
      } catch (e) {
        console.error(e)
        subscriber.error(e)
      }
    })
  }
  // List products and save on local
  async index (options) {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.remote.index(options)
        const products = response.data.products
        if (self.kioskConfig.STORE_LOCALLY === 1) {
          await Promise.all(products.map(async (product) => {
            await this.local.save(product)
          }))
        }
        resolve({products, meta: response.data.meta})
      } catch (e) {
        reject(e)
      }
    })
  }
}

export default new ProductsRepo()
