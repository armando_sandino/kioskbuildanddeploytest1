import axios from 'axios'
class API {
  http;
  constructor () {
    this.http = axios.create({
      baseURL: self.kioskConfig.API.URL + '/' + self.kioskConfig.API.CATALOG_ID,
      params: {
        token: self.kioskConfig.API.TOKEN
      }
    })
  }
  /**
   * Returns the product that belongs to the current kiosk
   * @param {page,per_page} pageconfig
   */
  getProducts (pageconfig = {page: 1, per_page: 25, sort_by: 'created_at'}) {
    return this.http.get('products', {
      params: pageconfig
    })
  }
  /**
   *  Returns a product base on its id
   * @param {*} productId Id of a product
   */
  getProduct (productId) {
    return this.http.get('products/' + productId)
  }
  /**
   * Returns a list of brands
   * @param {*} pageconfig  pagination sortby
   */
  getBrands (pageconfig = {page: 1, per_page: 9999, sort_by: 'name'}) {
    return this.http.get('brands', {
      params: pageconfig
    })
  }

  getProductsMinimal () {
    return this.http.get('products/minimal')
  }
  /**
   * Returns store categories
   */
  getCategories (params = {}) {
    return this.http.get('categories', {params: params})
  }
  /**
   * Returns store articles
   * @param {minimal:boolean} params of the request
   */
  getArticles (params = {minimal: true}) {
    return this.http.get('articles', {params: params})
  }
  /**
   * Return tags
   * @param {*} params
   */
  getTags (params = {featured_tags: true}) {
    return this.http.get('tags', {params: params})
  }
}

export default new API()
