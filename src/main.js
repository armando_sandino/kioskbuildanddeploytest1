/* eslint-disable no-undef */
import Vue from 'vue'
import axios from 'axios'
import vueConfig from 'vue-config'
import io from 'socket.io-client'
import Vue2TouchEvents from 'vue2-touch-events'
import VueFilter from 'vue-filter'
import vuexStore from './store/store.js'
// Use jQuery for easier DOM manipulation and animations

import $ from 'jquery'
import VueGlobalVariable from 'vue-global-var'
import App from './App'
import router from './router'
import Analytics from './analytics/analytics'
var retries = 0

console.log(
  '%cThe Peak Beyond App / %cVersion ' + process.app.version,
  'background: #00C796; color: #000;',
  'background: #00C796; color: #FFF;'
)

// Set jQuery globally for fancybox
window.jQuery = $

require('@fancyapps/fancybox')
require('../static/js/jquery.onScreenKeyboard.js')

Vue.config.productionTip = false

// Set axios base config
Vue.http = Vue.prototype.$http = axios.create({
  baseURL: self.kioskConfig.API.URL + '/' + self.kioskConfig.API.CATALOG_ID,
  params: {
    token: self.kioskConfig.API.TOKEN
  }
})

// Auto restart logic
var appRestartTimer = 10
window.appRestartInterval = setInterval(appRestart, 1000)

function appRestart () {
  if (appRestartTimer <= 0) {
    console.log("App didn't start, auto restart")
    location.reload(true)

    clearInterval(window.appRestartInterval)
  } else {
    let restartTimer = document.getElementById('restart-timer')
    if (restartTimer) {
      restartTimer.innerHTML =
        'The app will restart automatically in ' + appRestartTimer + 's.'
      appRestartTimer--
    }
  }
}

// Get store information before mounting the app
if (window.isSecureContext) {
  console.log('Secure context, use cache config')
  document.getElementById('output').innerHTML =
    'Secure context, use cache config'
  useCacheConfig()
} else {
  console.log('Not secure context, use online config')
  document.getElementById('output').innerHTML =
    'Not secure context, use online config'
  useOnlineConfig()
}

/**
 * Use cache config
 */
function useCacheConfig () {
  caches.open('kiosk-api-cache').then(function (cache) {
    var configURL =
      self.kioskConfig.API.URL +
      '/' +
      self.kioskConfig.API.CATALOG_ID +
      '/settings?token=' +
      self.kioskConfig.API.TOKEN

    cache.match(configURL).then(function (response) {
      if (response !== undefined) {
        response.json().then(function (data) {
          console.log('Init app with cache config.')
          document.getElementById('output').innerHTML =
            'Init app with cache config.'

          // Init APP with cache config
          initApp(data)
        })
      } else {
        console.log('No config in cache, use online config.')
        useOnlineConfig()
      }
    })
  })
}

/**
 * Use online config
 */
function useOnlineConfig () {
  // Try fetching the config online
  console.log('Fetching online config.')
  document.getElementById('output').innerHTML = 'Fetching online config.'

  Vue.http.get('settings').then(
    function (response) {
      console.log('Init app with online config.')
      document.getElementById('output').innerHTML =
        'Init app with online config.'

      // Init APP with remote config
      initApp(response.data)
      localStorage.setItem('config', JSON.stringify(response.data))
    },
    function (error) {
      console.log(error)
      // if and error happend check local config
      if (localStorage.getItem('config_data')) {
        // fetch local config and init app
        const cachedConfig = JSON.parse(localStorage.getItem('config_data'))
        initApp(cachedConfig)
      } else {
        // if no local config show error
        console.log(
          'Error while retreiving config from the API, waiting for the app to restart.',
          error.request
        )
        document.getElementById('output').innerHTML =
          'Error while retreiving config from the API, waiting for the app to restart.'
      }

      // Init APP without config
      // initApp(false)
    }
  )
}

/**
 * Initiate app with store config
 * @param  {json} store API response
 */
function initApp (data) {
  var mergedConfig = self.kioskConfig
  if (data !== false) {
    var store = data.store
    var catalog = data.catalog

    // Map remote config
    localStorage.setItem('config_data', JSON.stringify(data))
    var remoteConfig = {
      KIOSK_MODE: store.layout.template ? store.layout.template : 'shopping',
      HOME_LAYOUT: store.layout.home_layout
        ? store.layout.home_layout
        : 'default',
      HOME_SCREEN_TITLE: store.layout.home_screen_title
        ? store.layout.home_screen_title
        : "Today's Deals",
      PAGINATION_TIME: store.layout.pagination_time ? store.layout.pagination_time : 0,
      ON_SALE_TEXT: store.layout.checkout_text && store.layout.checkout_text !== '' ? store.layout.checkout_text : 'Discounts and promotions will be applied at the register.',
      ON_SALE_CATEGORY_ID: store.layout.store_category_id ? store.layout.store_category_id : null,
      ON_SALE_CATEGORY: store.layout.store_category ? store.layout.store_category : null,
      PRODUCT_UI: store.layout.nav_ui ? store.layout.nav_ui : 'regular',
      ON_SALE_BADGES_ENABLED: store.layout.on_sale_badges ? store.layout.on_sale_badges : false,
      STAND_SIDE: store.layout.stand_side ? store.layout.stand_side : 'left',
      SCREEN_TYPE: store.layout.screen_type
        ? store.layout.screen_type
        : 'big_screen',
      RFID_ENABLED: store.layout.rfid_disabled
        ? !store.layout.rfid_disabled
        : true,
      SHOPPING_ENABLED: store.layout.shopping_disabled
        ? !store.layout.shopping_disabled
        : true,
      POS_TYPE: catalog.api_type ? catalog.api_type : 'none',
      SENSOR_THRESHOLD: catalog.sensor_threshold
        ? catalog.sensor_threshold
        : 9999,
      SEARCH_SENSIBILITY: store.settings.search_sensibility
        ? store.settings.search_sensibility
        : 0.5,
      ORDER_NOTIFICATION: catalog.notify_by_email
        ? catalog.notify_by_email
        : false,
      CUSTOMER_NOTIFICATION: catalog.notify_to_customer
        ? catalog.notify_to_customer
        : false,
      MAIN_COLOR: store.settings.main_color
        ? store.settings.main_color
        : '#00C796',
      SECONDARY_COLOR: store.settings.secondary_color
        ? store.settings.secondary_color
        : '#E12291',
      BACKGROUND: store.settings.background_media
        ? store.settings.background_media.url
        : '/static/img/default-background.jpg',
      STORE_LOGO: store.logo
        ? store.logo.url
        : '/static/img/default-store-logo.png',
      LICENSE_NUMBER: store.settings.dispensary_license_number
        ? store.settings.dispensary_license_number
        : false,
      DISABLE_TAX_MESSAGE: store.settings.disable_tax_message
        ? store.settings.disable_tax_message
        : false,
      PICK_PRODUCT_ANIMATION: store.settings.show_pick_product_message
        ? store.settings.show_pick_product_message
        : false,
      SW_LOG: store.settings.service_worker_log
        ? store.settings.service_worker_log
        : false,
      TEXT: {
        PRODUCT_DESCRIPTION: store.settings.default_product_description
          ? store.settings.default_product_description
          : 'Please ask your budtender for more info.',
        WELCOME_MESSAGE: store.layout.welcome_message
          ? store.layout.welcome_message
          : null,
        PICK_PRODUCT: store.settings.pick_product_message
          ? store.settings.pick_product_message
          : 'The products below are featured near this display. Pick one, up check it out, and place it on the block to learn more.'
      },
      IDLE_DELAY: store.settings.idle_delay ? store.settings.idle_delay : 60,
      BLOCK_SIMULTANEUS_NFC: store.block_simultaneous_nfc ? store.block_simultaneous_nfc : false,
      RESTART_DELAY: store.settings.restart_delay
        ? store.settings.restart_delay
        : 30,
      REFRESH_DELAY: store.settings.refresh_delay
        ? store.settings.refresh_delay
        : 300,
      SORT_FEATURED: {
        BRANDS: store.settings.featured_products_on_top_for_brands_page
          ? store.settings.featured_products_on_top_for_brands_page
          : false,
        PRODUCTS: store.settings.featured_products_on_top_for_products_page
          ? store.settings.featured_products_on_top_for_products_page
          : false,
        USES: store.settings.featured_products_on_top_for_effects_and_uses_page
          ? store.settings.featured_products_on_top_for_effects_and_uses_page
          : false
      },
      HEAP_ID: store.settings.heap_id ? store.settings.heap_id : false,

      SLIDES: store.layout.store_assets,
      RFID_POP_UP_BEHAVIOR: store.settings.rfid_popup_setting ? store.settings.rfid_popup_setting : 0,
      LEFT_TO_RIGHT: store.settings.left_to_right ? store.settings.left_to_right : false
    }
    // Map navigation
    if (
      store.layout.navigation &&
      store.layout.navigation.items &&
      store.layout.navigation.items.length > 0
    ) {
      var nav = []

      store.layout.navigation.items.forEach(function (item) {
        var element = {
          label: item.label,
          path: item.link,
          order: item.order,
          title: item.title,
          description: item.description,
          image: false
        }

        if (item.asset) {
          element.image = item.asset.url
        }

        nav.push(element)
      })

      remoteConfig.NAV = nav
    } else {
      // Default nav
      remoteConfig.NAV = [
        { label: 'Products', path: '/products' },
        { label: 'Brands', path: '/brands' },
        { label: 'Effects + Uses', path: '/effects-uses' },
        { label: 'Featured products', path: '/featured-products' }
      ]
    }

    // Merge remote and local config
    mergedConfig = Object.assign(remoteConfig, self.kioskConfig)
  }

  // Set plugins
  Vue.use(vueConfig, mergedConfig)
  Vue.use(Vue2TouchEvents, {
    swipeTolerance: 100
  })
  Vue.use(VueFilter)
  // Global format price

  Vue.filter('formatPrice', function (value) {
    if (!value) value = 0
    value = '$' + Number(value).toFixed(2)
    return value
  })

  Vue.filter('formatPercentage', function (value) {
    if (!value) value = 0
    value = Number(value).toFixed(2) + '%'
    return value
  })
  // IF   GS LICENSE added and heap user added boot up GS LIBRARY
  if (self.kioskConfig.GS_LICENSE_KEY && mergedConfig.HEAP_USER) {
    // Boot-up the GS library and send information about the kiosk.
    loadGsScript(data)
  }

  /* eslint-disable no-new */
  var vm = new Vue({
    store: vuexStore,
    components: { App },
    router,
    template: '<App/>',
    created () {
      window.addEventListener('offline', () => {
        vuexStore.dispatch('setConnected', false)
      })
      window.addEventListener('online', () => {
        vuexStore.dispatch('setConnected', true)
      })
    }
  }).$mount('#app')

  // Socket initialisation for RFID reader
  console.log('%c connecting to sockket', 'background-color:#000; color:#fff')
  if (
    mergedConfig.RFID_ENABLED // &&
  //  window.location.href.indexOf(':8080') === -1
  ) {
    const socket = io('http://localhost:3000')
    var activeRfid = null

    socket.on('connect', function () {
      console.log('Connect ' + socket.id)
    })

    // Detect type
    socket.on('type', function (type) {
      if (type === 'us') {
        socket.emit('set_threshold', mergedConfig.SENSOR_THRESHOLD)
      }
    })

    // US sensor
    socket.on('sensor_uncovered', function (comName) {
      var portNumber = Number(comName.slice(-1))
      console.log('Sensor uncovered:' + portNumber)
      vm.$emit('sensor-uncovered', portNumber)
    })

    socket.on('sensor_covered', function (comName) {
      var portNumber = Number(comName.slice(-1))
      console.log('Sensor covered:' + portNumber)
      vm.$emit('sensor-covered', portNumber)
    })

    // RFID sensor
    const blockNFCmethod = (data) => {
      console.log(activeRfid ? `rfid event blocked ${data}` : `rfid trigged ${data}`)
      if (activeRfid === null) {
        activeRfid = data
        console.log('Tag:' + data)
        vm.$emit('tag-put', data)
        setTimeout(() => {
          console.log('NFC/RFID reader unblocked')
          activeRfid = null
        }, 15000)
      }
    }
    const doNotBlock = (data) => {
      if (activeRfid !== data) {
        activeRfid = data
        console.log('Tag:' + data)
        vm.$emit('tag-put', data)
      }
    }
    socket.on('tag_put', mergedConfig.BLOCK_SIMULTANEUS_NFC ? blockNFCmethod : doNotBlock)

    socket.on('tag_remove', function (data) {
      let condition = mergedConfig.BLOCK_SIMULTANEUS_NFC ? activeRfid === data : activeRfid !== null
      if (condition) {
        activeRfid = null
        console.log('Tag removed')
      }
    })
  }

  if ('serviceWorker' in navigator) {
    if (
      document.readyState === 'complete' ||
      document.readyState === 'interactive'
    ) {
      startSW(vm)
    } else {
      window.addEventListener('load', () => startSW(vm))
    }
  }
}

function loadGsScript (data) {
  try {
    /* var head = document.getElementsByTagName('head')[0]
    var gsScript = document.createElement('script')
    gsScript.type = 'text/javascript'
    gsScript.src = 'http://18.219.239.103:8000/static/js/gs_client.min.js'
    gsScript.id = 'gsclient'
    head.appendChild(gsScript)
    return new Promise((resolve, reject) => {
      gsScript.onload = function () {
        console.log('gsScript is loaded.')
        initGsClient(data)
      }
      gsScript.onerror = function () {
        head.removeChild(gsScript)
        setTimeout(loadGsScript, 5 * 60 * 1000, data)
      }
    }) */
    initGsClient(data)
  } catch (e) {
    console.error(e)
    /* head.removeChild(gsScript)
    setTimeout(loadGsScript, 5 * 60 * 1000, data) */
  }
}

function initGsClient (data) {
  console.log('DATA TO INIT', data)
  try {
    if (self.kioskConfig.ANALITYC_TOKEN) {
      Vue.use(VueGlobalVariable, {
        globals: {
          // eslint-disable-next-line no-undef
          $gsClient: new Analytics({
            'accountId': 1,
            'uploadFrequency': 5,
            'token': self.kioskConfig.ANALITYC_TOKEN,
            'source': {
              'store': data.store.name,
              'section': data.catalog.location,
              'kiosk': self.kioskConfig.HEAP_USER,
              'layout': data.store.layout.home_layout ? data.store.layout.home_layout : data.store.layout.template
            }
          })
        }
      })
    } else {
      console.error('NO HAY TOKEN')
    }

    console.log('$gsClient is up.')
  } catch (e) {
    console.error(e)
  }
}

/**
 * Start Service Worker
 */
function startSW (vm) {
  // Register service worker
  navigator.serviceWorker
    .register('/service-worker.js', { scope: '/' })
    .then(registration => {
      console.log('SW registered: ', registration)

      // Get service worker state
      var serviceWorker
      if (registration.installing) {
        serviceWorker = registration.installing
      } else if (registration.waiting) {
        serviceWorker = registration.waiting
      } else if (registration.active) {
        // Fetch data on active
        vm.$emit('fetch-data')
        window.serviceWorkerState = registration.active.state
      }

      if (serviceWorker) {
        serviceWorker.addEventListener('statechange', function (e) {
          window.serviceWorkerState = e.target.state
          if (e.target.state === 'activated') {
            // Fetch data on active
            vm.$emit('fetch-data')
          }
        })
      }
    })
    .catch(registrationError => {
      window.serviceWorkerState = 'registration error'
      console.log('SW registration failed: ', registrationError)
      retries = retries + 1
      if (retries < 4) {
        startSW(vm)
        console.log('Retrying SW Registration (' + retries + ')')
      }
    })

  // Check cache controller, ask SW to check cache regularly
  setInterval(function () {
    if (navigator.serviceWorker.controller) {
      navigator.serviceWorker.controller.postMessage('check cache')
    }
  }, 5 * 60 * 1000)
}
