// Const use to set time kiosk waits until next update
export const WAIT_TIME = 3 * 60000
export const RETRY_TIMES = 5
export const RETRY_LAPSUS = 2

export const RETRY_COOLDOWN = (RETRY_LAPSUS * 60000) / RETRY_TIMES

export const PRODUCTS_PAGE_SIZE = 25
