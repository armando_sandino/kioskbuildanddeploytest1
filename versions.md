## V1.6.0

### Changes
- 

## V1.5.0

### Changes
- New Layouts:  
  * CCC layout 
  * AdSales Layout
- Add Offline support
- Adds TPB Data Analytics 

### Bugfix:
  - Fix bug that create two  "Product view "analytics events
  - Fix bug that causes RFID products are show as out of stock
  