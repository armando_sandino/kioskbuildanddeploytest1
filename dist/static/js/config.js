self.kioskConfig = {

  DISABLE_SOCKET: true,

  /**
   * Kiosk mode
   * @type {String} brand|shopping|limited
   */
  /* /KIOSK_MODE:  "limited",

  /**
   * Home layout
   * @type {String} default|swipe|rfidswipe|rfidnav|swipenav
   */

  /**
  HOME_LAYOUT: 'default',  /**
   * Position of the stand, affects the tutorial animations in the app
   * @type {String} left|right
   */
  // STAND_SIDE: 'left',

  /**
   * Screen type
   * @type {String} big_screen|tablet
   */
  // SCREEN_TYPE: 'tablet',

  /**
   * Enable RFID functionality
   * @type {Boolean}
   */
  // RFID_ENABLED: true,

  /**
   * Enable shopping functionality
   * @type {Boolean}
   */
  // SHOPPING_ENABLED: true,

  /**
   * POS system used
   * @type {String} none|treez|flowhub|leaflogix
   */
  // POS_TYPE: 'none',

  /**
   * Sensor threshold
   * @type {Int}
   */
  // SENSOR_METHOD: 9999,

  /**
   * Search sensibility
   * @type {Number}
   */
  SEARCH_SENSIBILITY: 0.5,

  /**
   * Send order notification to admin
   * @type {Boolean}
   */
  // ORDER_NOTIFICATION: true,

  /**
   * Send order notification to customer (makes email field mandator)
   * @type {Boolean}
   */
  // CUSTOMER_NOTIFICATION: true,

  /**
   * UI colors
   * @type {String} Hex color
   */
  // MAIN_COLOR: '#00C796',
  // SECONDARY_COLOR: '#E12291',

  /**
   * Main background
   * @type {String} Media URL, can be an image or a video
   */
  // BACKGROUND: '/static/img/default-background.jpg',

  /**
   * Store logo
   * @type {String} Image URL, transparent PNG
   */
  // STORE_LOGO: '/static/img/default-store-logo.png',

  /**
   * API configuration
   * @param {String} URL
   * @param {String} CATALOG_ID
   * @param {String} TOKEN
   */
  API: {

    /* URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 141,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzIzODMyNTksInN1YiI6MTA2LCJhdWQiOlsiYXBpIl0sImp0aSI6ImNkOTdjODJmNTk2NmZjYzJmZThlZWQyZWViODRmNjZiIn0.Ha6muD_m3pKYShBKa0CUWvLI_hl3wsJiSNWQx9-V8zQ'
    */

    /* URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 101,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzM5MjM2MTMsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiODMwMjRmZjM5N2M2ODQ0ODQ1YTZjNmZkNDhlODNkNjQifQ.dLnWQTuTM4p_35bKbZS1aH20VUv7WqNj6cNL_mZIOMQ'
    */
    /*
    URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 41,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MTI4NzMwNTUsInN1YiI6MjgsImF1ZCI6WyJhcGkiXSwianRpIjoiZDJmZDIyNjI2ODU1OTI4NTI4NzliYTk3ZjU3NjE4MGUiLCJjYXRhbG9nX2lkIjoyNX0.1FVijmo3BQT5ueyiX4F4FHRRw5iDMGzajFXDaNm3-zY'
*/
    // Empire flower prod

    /*
    URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 101,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MzEzMzYyMDQsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiZTdkNjIxYWE4NDliODA5ODU2ZmU5NmE4ZjQyNGNmMDcifQ.1t9Nm0ANrOixk3VhNYx3iqSL79oMgwxJu6iuo5L1gzE'
*/

    /*
    URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 144,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzM2NzgzMDMsInN1YiI6MTA5LCJhdWQiOlsiYXBpIl0sImp0aSI6ImNkMjhhODJlYjY4ODM5OWY3ZGMwNmE4ODEwNzZkNTRlIn0.WsoxcxSGBvzcQFlFQfUv81vHg25pbnLI41htgwT4xVw'
*/
    /*
    URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 142,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzE1Nzg4MzIsInN1YiI6MTA3LCJhdWQiOlsiYXBpIl0sImp0aSI6IjAyMTM4MzE2Y2U0ZDY1ZGUyNTNmNWNlMzNmYjY3Y2ZiIn0.wz0Gl3ZVLtW-eMahvMNdI4KqJil6eRA_Uj0RVldBRkc'
    */
    /* URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 202,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NjM5MTk3MDMsInN1YiI6MTU4LCJhdWQiOlsiYXBpIl0sImp0aSI6IjQxOWI2MjZkZjEwNTQwNDUzODk2NDk3OThlODQ0OTRiIn0.OUHGyX9ESNkp1kZhXOYpT2D2A0mRTTC0iuxaQMPgyng'
    */
    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 27,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3ODAxNTMyNjEsInN1YiI6MzAsImF1ZCI6WyJhcGkiXSwianRpIjoiNjgzM2VmNDAzOWJkY2MwZDk2ZjQzY2RhZDIxZWZkZjcifQ.j0Gikh40FCCX7xN2hB-soUcqQ8EuX8YjlIGUF0cemPU'

    /* URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 244,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzYwNzY0OTMsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQ0MWM2YzhmMGQ0MjhmZjcxZTY3MThkMDUxOWNjZDUzIn0.xp64P8iITVDE7rkw_MPR2uxXLJT6oEafUygblLdXIxE'
    */
    /*
      URL: 'https://tpb-api.aimservices.tech/api/v1',
      CATALOG_ID: 248,
      TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzIyNjgzNDcsInN1YiI6MTY3LCJhdWQiOlsiYXBpIl0sImp0aSI6ImM5MzI4OTI4MTc3OGMyYjkxMTVkMTQyODRiMDYwNmFmIn0.wGBqtXQo4RZpLvZVG8ZZ4BsumqQaxogawctB9yfLO1k'
    */
    /* URL: 'https://tpb-api.aimservices.tech/api/v1',
    CATALOG_ID: 14,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzIyMDgyMzksInN1YiI6MTcsImF1ZCI6WyJhcGkiXSwianRpIjoiZDZiOGY1YzY1ZGZiZDY2NzBkZmQzY2ZjYzY1NTQzODkifQ.A-yHPmFNUeJJXd3MEoNXJE0t7_vLvIcjudTP61oERCY'
    */
    /*
    URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 244,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NjY4NTkzMTYsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQwNmRjODMxMWM4N2I1YTYzNTJhYjg3MDEzYTVhMDk0In0.5h5smTEoASSZAW9pfdv926HrxdTiZ6jz33NsJ5lzszc'
    */
    /* URL: 'https://tpb-api-stage.thepeakbeyond.com/api/v1',
    CATALOG_ID: 204,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3Njg1NjkwNzksInN1YiI6MTU3LCJhdWQiOlsiYXBpIl0sImp0aSI6ImQxZjMyODUxMzlhZmI2YzUwZGFiNWY1M2ZjMTU2YzIzIn0.LWPFXwU7vQKBl9pQw9kK0oRM-4RgKHkGtO6rRbJjdm8'
    */
    /* 'URL': 'https://api-prod.thepeakbeyond.com/api/v1',
    'CATALOG_ID': 83,
    'TOKEN': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MzEzMzYyMDQsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiZTdkNjIxYWE4NDliODA5ODU2ZmU5NmE4ZjQyNGNmMDcifQ.1t9Nm0ANrOixk3VhNYx3iqSL79oMgwxJu6iuo5L1gzE'
    */

    // 'URL': 'https://api-prod.thepeakbeyond.com/api/v1',
    // 'CATALOG_ID': 90,
    // 'TOKEN': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MzEzMzYyMDQsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiZTdkNjIxYWE4NDliODA5ODU2ZmU5NmE4ZjQyNGNmMDcifQ.1t9Nm0ANrOixk3VhNYx3iqSL79oMgwxJu6iuo5L1gzE'

    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 90,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3Nzk5NzE0MjgsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiMTUzMDA2Nzk2ZWMzMjQzYTgzNGNmMDQ5ODhjODIwZmEifQ.T0ghybQvw4p5LAfSy18m5mSZKdN2JZePNTzGwSHeEM4'

    /* URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 230,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NjY4NTkzMTYsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQwNmRjODMxMWM4N2I1YTYzNTJhYjg3MDEzYTVhMDk0In0.5h5smTEoASSZAW9pfdv926HrxdTiZ6jz33NsJ5lzszc'
    */

    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 230,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzYwNzY0OTMsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQ0MWM2YzhmMGQ0MjhmZjcxZTY3MThkMDUxOWNjZDUzIn0.xp64P8iITVDE7rkw_MPR2uxXLJT6oEafUygblLdXIxE'

    // // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 188,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MTY5MjM4NTgsInN1YiI6MzAsImF1ZCI6WyJhcGkiXSwianRpIjoiOGIwMzYwYTg1NTlhODQ4ZjU0NDEwMTc1NGU2MGY2YTMiLCJjYXRhbG9nX2lkIjoyN30.1ybGK2QxmHe4jia8HkzL9w6FCtS39bidwuUePUrrBQk'

    /* ----------- CCC banning prod ---------------- */
    /* URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 202,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NjM5MTk3MDMsInN1YiI6MTU4LCJhdWQiOlsiYXBpIl0sImp0aSI6IjQxOWI2MjZkZjEwNTQwNDUzODk2NDk3OThlODQ0OTRiIn0.OUHGyX9ESNkp1kZhXOYpT2D2A0mRTTC0iuxaQMPgyng'
*/
    /* ------------ Other kiosk  --------------- */
    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 244,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NzYwNzY0OTMsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQ0MWM2YzhmMGQ0MjhmZjcxZTY3MThkMDUxOWNjZDUzIn0.xp64P8iITVDE7rkw_MPR2uxXLJT6oEafUygblLdXIxE'

    /* ------------CCC BANNING DEV  --------------- */

    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 142,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3Nzg5MzAxOTQsInN1YiI6MTA3LCJhdWQiOlsiYXBpIl0sImp0aSI6Ijc4NzY0YWI5YjgwMzgxZjhmYjE3ODhmMzZiZDBjZTk1In0.nW9ArY8K8scTy6kkcrP610eJv-8F8WA2Gn8xwu_olPM'

    /* ------------PAX LIVE WELL  DEV  --------------- */
    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 142,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3ODAwNDc5ODQsInN1YiI6MTA3LCJhdWQiOlsiYXBpIl0sImp0aSI6IjU4NTI2YmYzODMzNTU2NTgyM2VjMzczODFjOTNmNDhmIn0.OrX14GpkcDpetcz-fkzAFjLMPmQNJ-3d08W3YjhbvEA'
    /* ------------EMPIRE CONNECT BIG SCREENF --------------- */
    URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    CATALOG_ID: 83,
    TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MzEzMzYyMDQsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiZTdkNjIxYWE4NDliODA5ODU2ZmU5NmE4ZjQyNGNmMDcifQ.1t9Nm0ANrOixk3VhNYx3iqSL79oMgwxJu6iuo5L1gzE'

    /* ------------EMPIRE CONNECT BIG SCREENF --------------- */
    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 243,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NjY4NTkzMTYsInN1YiI6MTYxLCJhdWQiOlsiYXBpIl0sImp0aSI6IjQwNmRjODMxMWM4N2I1YTYzNTJhYjg3MDEzYTVhMDk0In0.5h5smTEoASSZAW9pfdv926HrxdTiZ6jz33NsJ5lzszc'

    /* ------------GREEN GOODENSS --------------- */
    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 180,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NDgwMTM3NTQsInN1YiI6MTQ1LCJhdWQiOlsiYXBpIl0sImp0aSI6IjJlMzJlM2U4NWQyODlkYjg4MzZmZWE0NTkzOWQ1ZGUyIn0.iBuMp7PXledizEX4AB6wZWaeZdNDuhmNwtfryA9kIxc'
    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 180,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NDgwMTM3NTQsInN1YiI6MTQ1LCJhdWQiOlsiYXBpIl0sImp0aSI6IjJlMzJlM2U4NWQyODlkYjg4MzZmZWE0NTkzOWQ1ZGUyIn0.iBuMp7PXledizEX4AB6wZWaeZdNDuhmNwtfryA9kIxc'

    /* ------------GREEN GOODENSS --------------- */
    //  URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 180,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3NDgwMTM3NTQsInN1YiI6MTQ1LCJhdWQiOlsiYXBpIl0sImp0aSI6IjJlMzJlM2U4NWQyODlkYjg4MzZmZWE0NTkzOWQ1ZGUyIn0.iBuMp7PXledizEX4AB6wZWaeZdNDuhmNwtfryA9kIxc'

    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 90,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3ODI1NzU2MzIsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiN2YyMzMzM2ZhMjYzZjBmYTE5MDkxYTE4NThlYzhjODcifQ.Qv4urvZt_MpulbhGT6gXYfeMa5oTV6FrEn8xmNdz2aA'

    // Dev CCC long Beach Big SCREEN
    // URL: 'https://tpb-api.aimservices.tech/api/v1',
    // CATALOG_ID: 27,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3ODAxNTMyNjEsInN1YiI6MzAsImF1ZCI6WyJhcGkiXSwianRpIjoiNjgzM2VmNDAzOWJkY2MwZDk2ZjQzY2RhZDIxZWZkZjcifQ.j0Gikh40FCCX7xN2hB-soUcqQ8EuX8YjlIGUF0cemPU'

    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 27,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MTY5MjM4NTgsInN1YiI6MzAsImF1ZCI6WyJhcGkiXSwianRpIjoiOGIwMzYwYTg1NTlhODQ4ZjU0NDEwMTc1NGU2MGY2YTMiLCJjYXRhbG9nX2lkIjoyN30.1ybGK2QxmHe4jia8HkzL9w6FCtS39bidwuUePUrrBQk'

    // /* -------------     EMPIRE OLDER FLOWER 5         ---------------- */

    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 88,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3MzEzMzYyMDQsInN1YiI6NzEsImF1ZCI6WyJhcGkiXSwianRpIjoiZTdkNjIxYWE4NDliODA5ODU2ZmU5NmE4ZjQyNGNmMDcifQ.1t9Nm0ANrOixk3VhNYx3iqSL79oMgwxJu6iuo5L1gzE'

    /** *----CCC ARCADE PROD  ---- */

    // URL: 'https://api-prod.thepeakbeyond.com/api/v1',
    // CATALOG_ID: 245,
    // TOKEN: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjQ3Njk4NzM1OTgsInN1YiI6MTYyLCJhdWQiOlsiYXBpIl0sImp0aSI6ImY3ZDNkYThlNWVlZDY1YTM3MWY4ZTNlN2NjZWFjNTk0In0.NyUMb_CwQG5uyKs16mDfcuSPMmCeEWF2Ndb-Wv1ck-M'

  },

  /**
   * Dispensary license number
   * @type {String}
   */
  // LICENSE_NUMBER: 'A12-34-5678910-TEMP',

  /**
   * Disable tax message
   * @type {Boolean}
   */
  // DISABLE_TAX_MESSAGE: false,

  /**
   * Shop pick product animation
   * @type {Boolean}
   */
  // PICK_PRODUCT_ANIMATION: true

  /**
   * Enable service worker log in console
   * @type {Boolean}
   */
  SW_LOG: true,

  /**
   * Default UI texts
   * @param {String} PRODUCT_DESCRIPTION
   */
  // TEXT: {
  //   PRODUCT_DESCRIPTION: 'Please ask your budtender for more info.',
  //   WELCOME_MESSAGE: 'Browse our catalogue \nto see what we have \nin stock!',
  //   PICK_PRODUCT: 'The products below are featured near this display. Pick one, up check it out, and place it on the block to learn more.',
  // },

  /**
   * Idle delay, time before the inactivity modal appears
   * @type {Number}
   */
  // IDLE_DELAY: 60,

  /**
   * Restart delay, time before the session is refreshed when the inactivty modal is on
   * @type {Number}
   */
  // RESTART_DELAY: 30,

  /**
   * Refresh delay, time before the page is refreshed
   * @type {Number}
   */
  // REFRESH_DELAY: 300,

  /**
   * Put featured products on top on pages
   * @param {Boolean} BRANDS
   * @param {Boolean} PRODUCTS
   * @param {Boolean} USES
   */
  // SORT_FEATURED: {
  //   BRANDS: true,
  //   PRODUCTS: true,
  //   USES: true
  // },

  /**
   * GS Licence Key
   * @type {String}
   */
  GS_LICENSE_KEY: '1b9a1a1e-b04f-4638-a09e-7ce6f2014959',

  /**
   * HEAP account ID
   * @type {Number}
   */
  // HEAP_ID: 123456789,

  /**
   * HEAP user
   * @type {String}
   */
  HEAP_USER: 'dev', // PAGINATION_TIME:0

  /**
   * UI main navigation
   * @type {Array}
   */
  /*

  NAV: [
      // { label: 'Peanut Butter', path: '/product/167' },
    // { label: 'Cnuoncentrates', path: '/products/3' },
    // { label: 'Edibles', path: '/products/9' },
    // { label: 'Flowers', path: '/products/10' },
    // { label: 'Products', path: '/products' }
    { label: 'Products', path: '/products' },
    { label: 'Brands', path: '/brands' },
    { label: 'Education + Uses', path: '/effects-uses' },
    { label: 'Featured Products', path: '/featured-products' }
  ]
  */
  /*
   , NAV: [
     {
       description: null,
       image: "https://qph.fs.quoracdn.net/main-qimg-b09faade8bfe083e2f901cdb5bc60600",
       //image:'',
       label: "Joints",
       order: 1,
       path: "/products/469",
       title: "Joints",
     }, {
       description: null,
       image: null,
       label: "Test",
       order: 2,
       path: "/feature-products",
       title: "Teat",
     }
   ] */
  ANALITYC_TOKEN: '4|fciBDhlMsWfSTFptKWkqx5XDYd5Hjw3iRbszZiY7',
  ANALYTICS_API_URL: 'https://vulpecula.aimservices.tech/api/analytics',
  STORE_LOCALLY: 1,
  LEFT_TO_RIGHT: true
}
