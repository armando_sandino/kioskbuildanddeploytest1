# Front-end files, master branch

Run install command first:

```
npm install
``` 

Use build command to create dist files for production:

```
npm run build
```

Or dev command to launch a webpack dev server:

```
npm run dev
```

## For formating
Createa *.vscode* folder on the root of the project and add a *settings.json* containing the following

```js
{

  "eslint.validate": [
    "javascript",
    "html",
    "vue"
  ],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true,
    "source.fixAll.stylelint": true
  },
  "vetur.experimental.templateInterpolationService": false,
  "vetur.validation.template": false
}
```